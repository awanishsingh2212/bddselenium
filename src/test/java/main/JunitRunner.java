package main;

import com.opencsv.CSVWriter;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import steps.CommonStepDefinition;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/sampleAutomation.feature"
        ,glue= {"steps"},
        tags = {"@smoke"},
        monochrome = true
)
public class JunitRunner {
    static String path;
     static File file;


    @BeforeClass
    public static void generatePath() throws IOException {
        path=System.getProperty("user.dir")+"\\"+"report";
        SimpleDateFormat formatter = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
        Date date = new Date();
        path+=formatter.format(date).toString();
         file= new File(path);
        file.mkdir();

        System.out.println(path+"path");



    }
    @AfterClass
    public static  void printCount() throws IOException {
        FileWriter outputfile = new FileWriter(file+"//"+"report.csv");

        // create CSVWriter object filewriter object as parameter
        CSVWriter writer = new CSVWriter(outputfile);
        List ls=new LinkedList();

        // adding header to csv

        for (Map.Entry mp:CommonStepDefinition.map.entrySet())
        {

            outputfile.write(mp.getKey().toString());
            outputfile.write(mp.getValue().toString());
        }

        // closing writer connection
        writer.close();
        System.out.println(CommonStepDefinition.failedCount+""+CommonStepDefinition.PassCount);
    }
}
