package main;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;



@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/sampleAutomation.feature"
        ,glue= {"steps"},
        monochrome = true
)
public class runner {
}
