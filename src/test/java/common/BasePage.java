package common;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class BasePage {
    public   WebDriver driver;
    public void setDriver()
    {

    }
    public  WebDriver getDriver() throws MalformedURLException {

        ChromeOptions chromeOptions= new ChromeOptions();
        chromeOptions.addArguments("--disable-notifications");
        WebDriverManager.chromedriver().setup();
        driver=new ChromeDriver(chromeOptions);
//        URL url=new URL("http://localhost:4444/wd/hub");
////        driver=new RemoteWebDriver(url,chromeOptions);


        return driver;

    }
    public  WebDriver getDriver1(){


        WebDriverManager.firefoxdriver().setup();
        driver=new FirefoxDriver();
        return driver;

    }
}
