package steps;

import Pages.login;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.util.LinkedHashMap;
import java.util.Map;

public class CommonStepDefinition {
    public static Scenario scenario;
    login loginObject;
//    Scenario scenario;
    public static int PassCount=0;
    public static int failedCount=0;
    public static Map<String,String> map;

    public CommonStepDefinition()
    {
        loginObject=new login();
        map=new LinkedHashMap<>();



    }
    @Before
    public void initialize(Scenario scenario)
    {
        this.scenario=scenario;
    }
    @After
    public  void getthecount()
    {
        String Status="";
        if (scenario.isFailed())
        {
            failedCount++;
            Status="Fail";


        }
        else
        {
            PassCount++;
            Status="Pass";
        }
        map.put(scenario.getName(),Status);
    }
    @Given("^user is  on homepage$")
    public void user_is_on_homepage() throws Throwable {
        Assert.assertTrue(loginObject.launchUrl());

    }
    @Given("^user is  on homepage second$")
    public void user_is_on_homepage1() throws Throwable {
        Assert.assertTrue(loginObject.launchUrl2());

    }

//    @When("^user navigates to Login Page$")
//    public void user_navigates_to_Login_Page() throws Throwable {
//        Assert.assertTrue(loginObject.loginToPage());
//
//    }

    @When("^user enters username and Password$")
    public void user_enters_username_and_Password() throws Throwable {
        Assert.assertTrue(loginObject.logIn());

    }

//    @Then("^success message is displayed$")
//    public void success_message_is_displayed() throws Throwable {
//        Assert.assertTrue(loginObject.verifyMessage());
//
//    }
}
